@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Sucursales</div>

                <div class="card-body">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>Código</th>
                        <th>Sucursal</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($branches as $branch)
                        <tr>
                          <td>{{ $branch->id }}</td>
                          <td>{{ $branch->name }}</td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

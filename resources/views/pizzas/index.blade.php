@extends('layouts.app')

@section('content')
<html>
  <head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  </head>
  <body>
    <div class="container">
      <br />
      <h3 align="center">Agregar pizzas a la orden</h3>
      <br />
      <div class="table-responsive">
        <form method="post" id="add_pizzas">
          <span id="result"></span>
          <table class="table" id="user_table">
            <thead>
              <tr>
                <th width="35%">combination_id</th>
                <th width="35%">orden_id</th>
                <th width="10%">Action</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <tr>
              <td colspan="2" align="right">&nbsp;</td>
              <td>
                @csrf
                <input type="submit" name="save" id="save" class="btn btn-primary" value="Save" />
              </td>
            </tr>
            </tfoot>
          </table>
        </form>
      </div>
    </div>
  </body>
</html>

<script>
  $(document).ready(function(){
    var count = 1;

    add_pizzas(count);

    function add_pizzas(number)
    {
      html = '<tr>';
      html += '<td><input type="text" name="first_name[]" class="form-control" /></td>';
      html += '<td><input type="text" name="last_name[]" class="form-control" /></td>';
      if(number > 1)
      {
        html += '<td><button type="button" name="remove" id="" class="btn btn-danger remove">Remove</button></td></tr>';
        $('tbody').append(html);
      }
      else
      {   
          html += '<td><button type="button" name="add" id="add" class="btn btn-success">Add</button></td></tr>';
          $('tbody').html(html);
      }
    }

    $(document).on('click', '#add', function(){
      count++;
      add_pizzas(count);
    });

    $(document).on('click', '.remove', function(){
      count--;
      $(this).closest("tr").remove();
    });

   $('#add_pizzas').on('submit', function(event){
    event.preventDefault();
    $.ajax({
        url:'{{ route("pizza-inserts") }}',
        method:'post',
        data:$(this).serialize(),
        dataType:'json',
        beforeSend:function(){
            $('#save').attr('disabled','disabled');
        },
        success:function(data)
        {
            if(data.error)
            {
                var error_html = '';
                for(var count = 0; count < data.error.length; count++)
                {
                    error_html += '<p>'+data.error[count]+'</p>';
                }
                $('#result').html('<div class="alert alert-danger">'+error_html+'</div>');
            }
            else
            {
                add_pizzas(1);
                $('#result').html('<div class="alert alert-success">'+data.success+'</div>');
            }
            $('#save').attr('disabled', false);
        }
    })
   });
  });
</script>
@endsection
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Órdenes</div>

                <div class="card-body">
                  <a class="float-right btn btn-primary" href="/pizzas/index"> Crear Pizzas</a>
                  <br>
                  <br>
                  <table class="table">
                    <thead>
                      <tr>
                        <th>Código</th>
                        <th>Nombre</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($orders as $order)
                        <tr>
                          <td>{{ $order->id }}</td>
                          
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

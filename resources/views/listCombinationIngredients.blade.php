@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Combinación de ingredientes</div>

                <div class="card-body">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>Código</th>
                        <th>Tipo de pizza</th>
                        <th>Ingredientes</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($pizzaIngredients as $pizzaIngredient)
                        <tr>
                          <td>{{ $pizzaIngredient->id }}</td>
                          <td>{{ $pizzaIngredient->pizza->name }}</td>
                          <td>{{ $pizzaIngredient->ingredient->name }}</td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

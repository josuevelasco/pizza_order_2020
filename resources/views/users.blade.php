@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Clientes</div>

                <div class="card-body">
                  <ul>
                    <li><a href="/api/spending-customers">Clientes frecuentes</a></li>
                    <li><a href="/api/regular-customers">Clientes gastones</a></li>
                  </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

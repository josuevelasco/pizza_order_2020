@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Ingredientes</div>

                <div class="card-body">
                  <ul>
                    <li><a href="/api/combination-ingredients">Combinaciones de ingredientes</a></li>
                    <li><a href="/api/available-ingredients">Ingredientes disponibles</a></li>
                  </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

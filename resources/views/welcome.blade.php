<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Idea Pizza - ¿Listo para ordenar?</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                background-image: url("/images/bg-table2.jpg");
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            h2, h5{
                margin: 0 0 10px;
            }

            h2{
                background-color: #17a2b8;
                color: #fff;
                padding: 8px 30px;
            }
            h5{
                font-size: 50px;
                line-height: 50px;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            @media only screen and (max-width: 989px){
                .flex-center {
                    display: block;
                }
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
                width: 100%;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .btn-primary{
                background-color: #007bff;
                padding: 8px 20px !important;
                border-radius: 10px;
                color: #fff !important;
            }
            .login{
                background-color: #dc3545;
            }
            .register{
                background-color: #6c757d;
            }
            .gallery img{
                border-radius: 50%;
                width: auto;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a class="btn-primary login" href="{{ url('/home') }}">Crear pizza</a>
                    @else
                        <a class="btn-primary login" href="{{ route('login') }}">Iniciar sesión</a>

                        @if (Route::has('register'))
                            <a class="btn-primary register" href="{{ route('register') }}">Registrarme</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    <h2>Idea Pizza</h2>
                    @if (Auth::check())
                        <h5>Bienvenido {{ Auth::user()->name }}<br>, ¿Listo para ordenar?</h5>
                    @else
                        <h5>¿Listo para ordenar?<br> regístrate o inicia sesión.</h5>
                    @endif
                </div>

                <div class="gallery">
                    <img src="/images/pizza-intro.jpg">
                </div>
            </div>
        </div>
    </body>
</html>

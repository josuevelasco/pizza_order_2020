<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ingredient;
use App\Combination;
use App\CombinationIngredient;

class IngredientsController extends Controller
{
  public function index(){
    return view('ingredients');
  }

  function listCombinationIngredients(){
    $combinationIngredients = Combination
      ::Join('combination_ingredients', 'combinations.id', '=', 'combination_ingredients.combination_id')
      ->Join('ingredients', 'combination_ingredients.ingredient_id', '=', 'ingredients.id')
      ->where('is_default', true)
      ->select('combinations.name','combination_ingredients.combination_id', 'ingredients.name')
      ->get();

    return $combinationIngredients;
  }

  function listAvailableIngredients(){
    return Ingredient::all()->where('available', true);
  }
}

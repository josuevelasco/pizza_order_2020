<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UsersController extends Controller
{

  public function index(){
    return view('users');
  }

  function listRegularCustomers(){
    return User::all();
    // Consulta de usuarios no admin sorteados descendente de acuerdo a las ordenes
  }

  function listSpendingCustomers(){
    return User::all()->where('is_admin', true);
    // Consulta de usuarios no admin sorteados descendente de acuerdo a la suma total de sus ordenes
  }
}

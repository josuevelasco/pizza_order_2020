<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;

class OrdersController extends Controller
{
  function index(){
    $orders = Order::all();
    return view('orders', ['orders' => $orders]);
  }
}

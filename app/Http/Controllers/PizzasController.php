<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pizza;
use App\Combination;
use Validator;

class PizzasController extends Controller
{

  function index()
  {
    return view('/pizzas/index');
    $combinationList = Combination::lists('name', 'id');
  }
  
  function insert(Request $request)
  {
    if($request->ajax()){
      $rules = array(
       'combination_id.*'  => 'required',
       'order_id.*'  => 'required'
      );
      $error = Validator::make($request->all(), $rules);

      if($error->fails())
      {
       return response()->json([
        'error'  => $error->errors()->all()
       ]);
      }

      $combination_id = $request->combination_id;
      $order_id = $request->order_id;
      for($count = 0; $count < count($combination_id); $count++)
      {
       $data = array(
        'combination_id' => $combination_id[$count],
        'order_id'  => $order_id[$count]
       );
       $insert_data[] = $data; 
      }

      Pizza::insert($insert_data);
      return response()->json([
       'success'  => 'Agregado.'
      ]);
    }
  }
}

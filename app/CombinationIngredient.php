<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CombinationIngredient extends Model
{
  public function combination()
  {
      return $this->belongsTo('App\Combination');
  }

  public function ingredient()
  {
      return $this->belongsTo('App\Ingredient');
  }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Combination extends Model
{
  public function combinationIngredients()
  {
    return $this->hasMany('App\CombinationIngredient');
  }

  public function pizzas()
  {
    return $this->hasMany('App\Pizza');
  }
}

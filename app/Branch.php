<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
  function orders()
  {
    return $this->hasMany('App\Order');
  }
}

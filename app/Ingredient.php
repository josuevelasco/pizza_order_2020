<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
  public function combinationIngredients()
  {
    return $this->hasMany('App\CombinationIngredient');
  }
}

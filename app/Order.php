<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
  function pizzas()
  {
    return $this->hasMany('App\Pizza');
  }
}

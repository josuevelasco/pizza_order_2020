<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/users', 'UsersController@index')->name('users');
Route::get('/ingredients', 'IngredientsController@index')->name('ingredients');
Route::get('/branches', 'BranchesController@index')->name('branches');
Route::get('/orders', 'OrdersController@index')->name('orders');

Route::get('/pizzas/index', 'PizzasController@index')->name('pizza-index');
Route::post('/pizzas/insert', 'PizzasController@insert')->name('pizza-inserts');


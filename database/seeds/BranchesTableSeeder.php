<?php

use Illuminate\Database\Seeder;

class BranchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('branches')->insert([
        'name' => 'Sucursal Merliot'
      ]);
      DB::table('branches')->insert([
        'name' => 'Sucursal Proceres'
      ]);
      DB::table('branches')->insert([
        'name' => 'Sucursal Heroes'
      ]);
    }
}

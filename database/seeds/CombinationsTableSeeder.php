<?php

use Illuminate\Database\Seeder;

class CombinationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('combinations')->insert([
        'name' => 'Pizza Italia',
        'price' => 3.00
      ]);
      DB::table('combinations')->insert([
        'name' => 'Pizza 5 Quesos',
        'price' => 3.50
      ]);
      DB::table('combinations')->insert([
        'name' => 'Pizza Suprema',
        'price' => 4.00
      ]);
      DB::table('combinations')->insert([
        'name' => 'Pizza Personalizada',
        'price' => 3.00
      ]);
    }
}

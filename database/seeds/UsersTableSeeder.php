<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
        'name' => 'Administrador',
        'email' => 'info@ideapizza.com',
        'is_admin' => 1,
        'password' => bcrypt('info@ideapizza.com')
      ]);

      DB::table('users')->insert([
        'name' => 'Josue Velasco',
        'email' => 'josuevelasco.ing@gmail.com',
        'is_admin' => 0,
        'password' => bcrypt('josuevelasco.ing@gmail.com')
      ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class CombinationIngredientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // PIZZAS ITALIANAS
      DB::table('combination_ingredients')->insert([
        'is_default' => 1,
        'combination_id' => 1,
        'ingredient_id' => 1
      ]);
      DB::table('combination_ingredients')->insert([
        'is_default' => 1,
        'combination_id' => 1,
        'ingredient_id' => 2
      ]);
      DB::table('combination_ingredients')->insert([
        'is_default' => 1,
        'combination_id' => 1,
        'ingredient_id' => 3
      ]);

      // PIZZAS 5 QUESOS
      DB::table('combination_ingredients')->insert([
        'is_default' => 1,
        'combination_id' => 2,
        'ingredient_id' => 4
      ]);
      DB::table('combination_ingredients')->insert([
        'is_default' => 1,
        'combination_id' => 2,
        'ingredient_id' => 5
      ]);

      // PIZZAS SUPREMAS
      DB::table('combination_ingredients')->insert([
        'is_default' => 1,
        'combination_id' => 3,
        'ingredient_id' => 1
      ]);
      DB::table('combination_ingredients')->insert([
        'is_default' => 1,
        'combination_id' => 3,
        'ingredient_id' => 2
      ]);
      DB::table('combination_ingredients')->insert([
        'is_default' => 1,
        'combination_id' => 3,
        'ingredient_id' => 3
      ]);
      DB::table('combination_ingredients')->insert([
        'is_default' => 1,
        'combination_id' => 3,
        'ingredient_id' => 4
      ]);
      DB::table('combination_ingredients')->insert([
        'is_default' => 1,
        'combination_id' => 3,
        'ingredient_id' => 5
      ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class IngredientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('ingredients')->insert([
        'name' => 'Peperonni',
        'available' => 1,
        'cost' => 0.30
      ]);
      DB::table('ingredients')->insert([
        'name' => 'Jamon',
        'available' => 1,
        'cost' => 0.20
      ]);
      DB::table('ingredients')->insert([
        'name' => 'Salami',
        'available' => 1,
        'cost' => 0.40
      ]);
      DB::table('ingredients')->insert([
        'name' => 'Queso mozarella',
        'available' => 1,
        'cost' => 0.50
      ]);
      DB::table('ingredients')->insert([
        'name' => 'Queso parmesano',
        'available' => 1,
        'cost' => 0.45
      ]);
      DB::table('ingredients')->insert([
        'name' => 'Champiñones',
        'available' => 0,
        'cost' => 0.35
      ]);
    }
}

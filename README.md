# Pasos para instalación de proyecto

PHP 7.2.24 y MySQL 14.14 Distrib 5.7.29

$ git clone git@gitlab.com:josuevelasco/pizza_order_2020.git   //Clonar proyecto
$ cd pizza_order_2020/                                         //Ubicarse en carpeta del proyecto
$ composer install                                             //Instalar dependencias
Crear base de datos 'pizza_order' en mysql                     //Crear base de datos del proyecto
Crear archivo .env usando el ejemplo del archivo .env.example, agregar 'pizza_order' en DB_DATABASE, modificar DB_USERNAME y DB_PASSWORD de acuerdo a las credenciales de su gestor bd
$ php artisan migrate:fresh --seed                             //Ejecutar migraciones y semillas
$ php artisan key:generate                                     //Generar llave para iniciar el proyecto
$ php artisan serve                                            //Iniciar proyecto http://localhost:8000/

# Generalidades

## Existen dos usuarios después de generar las semillas, un administrador y un cliente, los accesos:
* Admin:   user => 'info@ideapizza.com', pass => 'info@ideapizza.com'
* Cliente: user => 'josuevelasco.ing@gmail.com', pass => 'josuevelasco.ing@gmail.com'

* Debe iniciar sesión para acceder a las opciones, aunque se pueden registrar usuarios pero con el rol de cliente.
* Algunas opciones no se visualizan en el usuario cliente.